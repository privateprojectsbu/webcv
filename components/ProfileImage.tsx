

const ProfileImage = () => {
const image_uuid = "218814ac-b04e-4156-9be9-3770ff825a70"

  return <img className="rounded-full w-36 h-36 lg:h-56 lg:w-56 border-4 border-black dark:border-yellow-50"
              src={"https://cms.tecios.de/assets/"+image_uuid}/>

}

export default ProfileImage;